provider "aws" {
  profile = local.manifest.project.profile
  region  = local.manifest.project.region
}

locals {
  manifest = yamldecode(file("${path.cwd}/manifest.yaml"))
}

module "module_usage_howto" {
  source    = "./../.."
  project   = local.manifest.project
  resources = local.manifest.resources
}
