output "alb_endpoint" {
  description = "Public DNS of ALB"
  value       = aws_lb.alb.dns_name
}

output "instances_ids" {
  description = "Ids of the created EC2 instances."
  value = [
    for instance in aws_instance.blue_instance : instance.id
  ]
}

output "instances_arns" {
  description = "Arns of the created EC2 instances."
  value = [
    for instance in aws_instance.blue_instance : instance.arn
  ]
}

output "instances_ips" {
  description = "List of public IP addresses assigned to the instances, if applicable."
  value = [
    for instance in aws_instance.blue_instance : instance.public_ip
  ]
}

output "debug" {
  description = "For debug purpose."
  value       = var.resources
}
