locals {
  resources = {
    name                          = var.resources.name
    vpc_id                        = var.resources.vpc_id
    blue_user_data_file           = try(var.resources.blue_user_data_file, null)
    green_user_data_file          = try(var.resources.green_user_data_file, null)
    instances_security_groups_ids = var.resources.instances_security_groups_ids
    alb_security_groups_ids       = var.resources.alb_security_groups_ids
    alb_subnets_ids               = toset(var.resources.alb_subnets_ids)
    traffic_distribution          = var.resources.traffic_distribution
    blue_ami                      = var.resources.blue_ami
    green_ami                     = var.resources.green_ami
    blue_instance_type            = var.resources.blue_instance_type
    green_instance_type           = var.resources.green_instance_type
    tags                          = try(var.resources.tags != null ? var.resources.tags : null, null)
  }

  traffic_dist_map = {
    blue = {
      blue  = 100
      green = 0
    }
    blue-90 = {
      blue  = 90
      green = 10
    }
    split = {
      blue  = 50
      green = 50
    }
    green-90 = {
      blue  = 10
      green = 90
    }
    green = {
      blue  = 0
      green = 100
    }
  }


  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

resource "aws_lb" "alb" {
  desync_mitigation_mode           = "defensive"
  drop_invalid_header_fields       = false
  enable_cross_zone_load_balancing = true
  enable_deletion_protection       = true
  enable_http2                     = true
  enable_waf_fail_open             = false
  idle_timeout                     = 60
  internal                         = false
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  security_groups                  = local.resources.alb_security_groups_ids
  subnets                          = local.resources.alb_subnets_ids
}

resource "aws_alb_listener" "default" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "forward"
    forward {
      target_group {
        arn    = aws_alb_target_group.blue.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "blue", 100)
      }

      target_group {
        arn    = aws_alb_target_group.green.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "green", 0)
      }

      stickiness {
        enabled  = true
        duration = 60
      }
    }
  }

  depends_on = [aws_lb.alb]
}

resource "aws_alb_target_group" "blue" {
  connection_termination             = false
  deregistration_delay               = "300"
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }
}

resource "aws_alb_target_group" "green" {
  connection_termination             = false
  deregistration_delay               = "300"
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }
}

resource "aws_instance" "blue_instance" {
  for_each                    = local.resources.alb_subnets_ids
  ami                         = local.resources.blue_ami
  instance_type               = local.resources.blue_instance_type
  subnet_id                   = each.value
  vpc_security_group_ids      = local.resources.instances_security_groups_ids
  associate_public_ip_address = true

  user_data = local.resources.blue_user_data_file != null ? templatefile(local.resources.blue_user_data_file, {}) : null

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "BLUE_INSTANCE_${each.key} - ALB ${local.resources.name}"
  })
}

resource "aws_lb_target_group_attachment" "blue_instance_attach" {
  for_each         = local.resources.alb_subnets_ids
  target_group_arn = aws_alb_target_group.blue.arn
  target_id        = aws_instance.blue_instance[each.key].id
  port             = 80
}

resource "aws_instance" "green_instance" {
  for_each                    = local.resources.alb_subnets_ids
  ami                         = local.resources.green_ami
  instance_type               = local.resources.green_instance_type
  subnet_id                   = each.value
  vpc_security_group_ids      = local.resources.instances_security_groups_ids
  associate_public_ip_address = true

  user_data = local.resources.green_user_data_file != null ? templatefile(local.resources.green_user_data_file, {}) : null

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "GREEN_INSTANCE_${each.key} - ALB ${local.resources.name}"
  })
}

resource "aws_lb_target_group_attachment" "green_instance_attach" {
  for_each         = local.resources.alb_subnets_ids
  target_group_arn = aws_alb_target_group.green.arn
  target_id        = aws_instance.green_instance[each.key].id
  port             = 80
}
