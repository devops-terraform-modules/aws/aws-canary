REPOSITORY="quay.io/terraform-docs/terraform-docs:0.17.0"
SOURCE="."
TARGET="/workspace"
MOUNT="${SOURCE}:${TARGET}"

clean:
	rm -fr .terraform* *tfstate*
init:
	terraform init
validate:
	terraform validate
plan:
	terraform plan
apply:
	terraform apply
aapply:
	terraform apply --auto-approve
destroy:
	terraform destroy
adestroy:
	terraform destroy --auto-approve
output:
	terraform output
format:
	terraform fmt  -write=true -diff -recursive ./  
docs:
	docker run --rm --volume $(MOUNT) ${REPOSITORY} markdown document --output-file=README.md  --show all --read-comments --recursive-path=./example/  ${TARGET}  
sec:
	docker run --rm -it -v $(MOUNT) aquasec/tfsec --format=json ${TARGET} > tfsec_report.json
lint:
	docker run --rm -v ".:/data" -t ghcr.io/terraform-linters/tflint --recursive -f compact
